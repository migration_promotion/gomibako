module.exports = function(RED) {
    //. ノードの処理内容    
    var noble = require('noble');
    function NakaNode(config) {
        RED.nodes.createNode(this,config);
        
        this.filter = config.filter;
        this.serviceuuid = config.serviceuuid;
        this.charactaristicsuuid = config.charactaristicsuuid;
        this.charactaristicsuuid2 = config.charactaristicsuuid2;
        
        var node = this;
        this.on('input', function(msg) {
            console.log('node-red on : ' + noble.state);

            noble.on('stateChange', function(state) {
                console.log('stateChange Function : ' + state);
                if (state === 'poweredOn') {
                    noble.startScanning();
                } else {
                    noble.stopScanning();
                    //noble.removeAllListeners();
                }
            });
            
            noble.on('scanStart', function() {
                console.log('on -> scanStart');
                node.status({fill:"green",shape:"dot",text:"started"});
            });

            noble.on('scanStop', function() {
                console.log('on -> scanStop');
                node.status({fill:"red",shape:"ring",text:"stopped"});
            });

            noble.on('discover', function(peripheral) {
                if (peripheral.uuid == node.filter) {
                    noble.stopScanning();
                    peripheral.connect(function(error) {
                        console.log('connected to peripheral: ' + peripheral.uuid);
                        // Discover Services
                        peripheral.discoverServices(node.serviceuuid, function(error, services) {
                            console.log('discovered the following services:' );
                            for (var i in services) {
                                console.log('  ' + i + ' uuid: ' + services[i].uuid);
                            }
                            
                            var service = services[0];
                            service.discoverCharacteristics([node.charactaristicsuuid,node.charactaristicsuuid2],function(error,characteristics) {
                                for (var i in characteristics) {
                                    console.log('  ' + i + ' char uuid: ' + characteristics[i].uuid);
                                }
                                
                                var charData = characteristics[0];
                                charData.on('data', function(data, isNotification) {
                                    console.log('characteristics data is now : ', data.readUInt8(0));
                                    msg.serviceUuid = charData._serviceUuid;
                                    msg.charUuid = charData.uuid;
                                    msg.payload = data.readUInt8(0);
                                    node.send(msg);
                                });

                                // to enable notify
                                charData.subscribe(function(error) {
                                  console.log('notification on');
                                });
                                
                                var charData2 = characteristics[1];
                                charData2.on('data', function(data, isNotification) {
                                    console.log('characteristics data is now : ', data.readUInt8(0));
                                    msg.serviceUuid = charData2._serviceUuid;
                                    msg.charUuid = charData2.uuid;
                                    msg.payload = data.readUInt8(0);
                                    node.send(msg);
                                });

                                // to enable notify
                                charData2.subscribe(function(error) {
                                  console.log('notification2 on');
                                });
                            });
                        });
                    });
                }
                
                // disconnect
                peripheral.on('disconnect', function() {
                    console.log('on -> disconnect');
                    noble.startScanning();
                    //process.exit(0);
                });
            });
        });
    }
    //. ノードとして登録
    RED.nodes.registerType("NakaNode",NakaNode);
};


