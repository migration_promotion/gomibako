module.exports = function(RED) {
    //. ノードの処理内容    
    var noble = require('noble');
    var requestCount = 0;
    var gwname = '';
    var appkey = '';
    // 時間のログを出力するためのもの
    var d = new Date();
    var year  = d.getFullYear();
    var month = d.getMonth() + 1;
    var day   = d.getDate();
    var hour  = ( d.getHours()   < 10 ) ? '0' + d.getHours()   : d.getHours();
    var min   = ( d.getMinutes() < 10 ) ? '0' + d.getMinutes() : d.getMinutes();
    var sec   = ( d.getSeconds() < 10 ) ? '0' + d.getSeconds() : d.getSeconds();

    function advertise(config) {
        RED.nodes.createNode(this,config);
        this.filter = config.filter;
        this.serviceuuid = config.serviceuuid;
        this.charactaristicsuuid = config.charactaristicsuuid;
        this.charactaristicsuuid2 = config.charactaristicsuuid2;
        this.GWName = config.GWName;
        gwname = config.GWName;
        this.appKey = config.appKey;
        appkey = config.appKey
        
        var node = this;
        this.on('input', function(msg) {
            console.log(logTime() + 'node-red on : ' + noble.state);
            msg.GWName = this.GWName;
            msg.appKey = this.appKey;

            noble.on('stateChange', function(state) {
                console.log(logTime() + 'stateChange Function : ' + state);
                if (state === 'poweredOn') {
                    noble.startScanning();
                } else {
                    noble.stopScanning();
                    noble.removeAllListeners();
                }
            });
            
            noble.on('scanStart', function() {
                console.log(logTime() + 'on -> scanStart');
                node.status({fill:"green",shape:"dot",text:"started"});
            });

            noble.on('scanStop', function() {
                console.log(logTime() + 'on -> scanStop');
                noble.removeAllListeners();
                node.status({fill:"red",shape:"ring",text:"stopped"});
            });

            noble.on('discover', function(peripheral) {
                if (peripheral.uuid == node.filter) {
                    noble.stopScanning();
                    console.log(logTime() + 'uuid ' + node.filter);
                    peripheral.connect(function(error) {
                        console.log(logTime() + 'connected to peripheral: ' + peripheral.uuid);
                        // Discover Services
                        peripheral.discoverServices(null, function(error, services) {
                            console.log(logTime() + 'discovered the following services:' );
                            for (var i in services) {
                                console.log(logTime() + '  ' + i + ' uuid: ' + services[i].uuid);
                            }
                            var service = services[2];
                            service.discoverCharacteristics([node.charactaristicsuuid,node.charactaristicsuuid2],function(error,characteristics) {
                                for (var i in characteristics) {
                                    console.log(logTime() + '  ' + i + ' char uuid: ' + characteristics[i].uuid);
                                }
                                var charData = characteristics[0];
                                charData.on('data', function(data, isNotification) {
                                    console.log(logTime() + 'characteristics data is now : ', data.readUInt8(0));
                                    msg.serviceUuid = charData._serviceUuid;
                                    msg.charUuid = charData.uuid;
                                    msg.payload = data.readUInt8(0);
                                    msg.type = 'ultrasonic';
                                    sendDist(data.readUInt8(0));
                                    node.send(msg);
                                    requestCount += 1;
                                });

                                // to enable notify
                                charData.subscribe(function(error) {
                                  console.log(logTime() + 'notification on');
                                });
                            });
                            var service2 = services[3];
                            service2.discoverCharacteristics(['2a19'],function(error,characteristics) {
                                for (var i in characteristics) {
                                    console.log(logTime() + '  ' + i + ' char uuid battery: ' + characteristics[i].uuid);
                                }
                                
                                var charData2 = characteristics[0];
                                charData2.on('data', function(data, isNotification) {
                                    console.log(logTime() + 'characteristics data is now battery: ', data.readUInt8(0));
                                    msg.serviceUuid = charData2._serviceUuid;
                                    msg.charUuid = charData2.uuid;
                                    msg.payload = data.readUInt8(0);
                                    msg.type = 'battery';
                                    sendVolt(data.readUInt8(0));
                                    node.send(msg);
                                    requestCount += 1;
                                });

                                // to enable notify
                                charData2.subscribe(function(error) {
                                  console.log(logTime() + 'notification on');
                                });
                            });
                        });
		            });
                }
                // disconnect
                peripheral.on('disconnect', function() {
                    console.log(logTime() + 'on -> disconnect');
                    noble.startScanning();
                    if (requestCount == 0) {
                        process.exit(1);
                    }
                });
            });
        });
    }
    
    function sendDist (dist) {
        dist = 90 - dist;
        if (dist < 0) {
            dist = -10;
        }
        
        var sensorData = {};
        sensorData.timestamp = new Date().toISOString();
        sensorData = {
            sdata:{
                n01: dist
            }
        }
        console.log(logTime + ':' + sensorData['sdata']['n01']);
        SendDataToToami(sensorData);
        return;
    }
    function sendVolt (volt) {
        var sensorData = {};
        sensorData.timestamp = new Date().toISOString();
        sensorData = {
            sdata:{
                n02: volt
            }
        }
        console.log(logTime + ':' + sensorData['sdata']['n02']);
        SendDataToToami(sensorData);
        return;
    }
    var request = require("request");
// send to toami
function SendDataToToami(data){
    var headers = {
        "appkey":appkey,
        "Content-Type":"application/json;charset=UTF-8",
        "Accept":"application/json"
    };
    var options = {
        url: "https://www.to4do.com/Thingworx/Things/" + gwname + "/Properties/sdata",
        method: "PUT",
        headers: headers,
        json: data
    };
    console.log(options['url']);
    request(options, function(error,response){
        console.log(logTime + 'responce code::' + response.statusCode);
        process.exit(1);
    });
}

    function logTime() {
        var d = new Date();
        var year  = d.getFullYear();
        var month = d.getMonth() + 1;
        var day   = d.getDate();
        var hour  = ( d.getHours()   < 10 ) ? '0' + d.getHours()   : d.getHours();
        var min   = ( d.getMinutes() < 10 ) ? '0' + d.getMinutes() : d.getMinutes();
        var sec   = ( d.getSeconds() < 10 ) ? '0' + d.getSeconds() : d.getSeconds();
        var logTime = '[' + year + '-' + month + '-' + day + ' ' + hour + ':' + min + ':' + sec + ']';
        return logTime;
    }
    //. ノードとして登録
    RED.nodes.registerType("advertise",advertise);
};
