/*****

dcm5156
*****/

var noble = require('noble');
var DEVICE_NAME = "PERCUL01";
var SERVICE_UUID = '82a49d446d0411e8adc0fa7ae01bbebc';
//超音波
var SERVICE_CHARACTERISTIC_UUID2 = "82a4a0006d0411e8adc0fa7ae01bbebc";
//赤外線
var SERVICE_CHARACTERISTIC_UUID = "82a4a4f66d0411e8adc0fa7ae01bbebc";
// センサーの値を入れる箱
var ultrasonic = null;
var infraRed = null;


var getSensorData = function(callback) {
    noble.on('stateChange', function(state) {
        if (state === 'poweredOn') {
            noble.startScanning();
        } else {
            noble.stopScanning();
        }
    });

    noble.on('discover', function(peripheral) {
        if(peripheral.uuid == '001ec06514aa'){
            peripheral.connect(function(error) {
                console.log('connected to peripheral: ' + peripheral.uuid);
                peripheral.discoverServices(SERVICE_UUID, function(error, services) {
                    var deviceInformationService = services[0];
                    var result = "";
                    deviceInformationService.discoverCharacteristics(null, function(error, characteristics) {
                        console.log('discovered the following characteristics:');
                        for(j = 0; j < characteristics.length; j++) {
                            if(SERVICE_CHARACTERISTIC_UUID2  == characteristics[j].uuid ){
                                console.log( j + ": SERVICE_CHARACTERISTIC_UUID2 exist!!");
                                characteristics[j].on('data', function(data, isNotification) {
                                    result += data[0];
                                    console.log('ultrasonic:' + result);
                                    if (result > 90) {
                                        console.log(result + ": 90以上のデータが入力されたのでプログラムを終了します");
                                        process.exit(0);
                                    } else if (result == "") {
                                        console.log("データが入力されていないのでプログラムを終了します");
                                        process.exit(0);
                                    }
                                    ultrasonic = result;
                                    testFunction(result);
                                });
                                characteristics[j].subscribe(function(error) {
                                    console.log('notify');
                                });
                            }
                            if(SERVICE_CHARACTERISTIC_UUID  == characteristics[j].uuid ){
                                console.log( j + ": SERVICE_CHARACTERISTIC_UUID exist!!");
                                characteristics[j].on('data', function(data, isNotification) {
                                    result = 0
                                    for(var i=0; i<data.length; i++){
                                        result += data[i];
                                    }
                                    console.log('data0' + data[0]);
                                    console.log('infraRed:' + data[0]);
                                    infraRed = result;
                                });
                                characteristics[j].subscribe(function(error) {
                                    console.log('notify');
                                    // testFunction(result);
                                });
                                if (ultrasonic !== null && infraRed !== null) {
                                    callback([ultrasonic, infraRed]);
                                }
                            }
                        }
                        //return [ultrasonic, infraRed];
                    });
                });
            });

        }
    });
}
getSensorData(function(sensorData){
    console.log('getSensorData');
    ultrasonic = sensorData[0];
    infraRed = sensorData[1];
    testFunction2(ultrasonic, infraRed);
});

function testFunction (result) {
    var data = result.replace(",","");
    sensorData = {};
    sensorData.timestamp = new Date().toISOString();
    console.log('データ:' + data);
          sensorData = {
                sdata:{
                        n01: 90 - result,
                        n02: infraRed
                }
          }
    console.log('超音波:' + sensorData['sdata']['n01']);
    console.log('赤外線:' + sensorData['sdata'['n02']]);
    SendDataToToami(sensorData);
    return;
}

function testFunction2 (ultrasonic, infraRed) {
    //var data = result;
    var sensorData = {};
    sensorData.timestamp = new Date().toISOString();
    //console.log('データ:' + data);
    sensorData = {
        sdata:{
            n01: 90 - ultrasonic,
            n02: infraRed
        }
    }
    console.log('超音波:' + sensorData['sdata']['n01']);
    console.log('赤外線:' + sensorData['sdata'['n02']]);
    SendDataToToami(sensorData);
    return;
}

//  ------- Toamiに投げる部分 --------
// ↓の行を追加
var request = require("request");
// send to toami
function SendDataToToami(data){
    var headers = {
        "appkey":"acc38dde-fdfa-4ab6-a699-7b4e72166727",
        "Content-Type":"application/json;charset=UTF-8",
        "Accept":"application/json"
    };
    var options = {
        url: "https://demo01.to4do.com/Thingworx/Things/Thg_GW_kyxa_9xlt/Properties/sdata",
        method: "PUT",
        headers: headers,
        json: data
    };

    request(options, function(error,response){
        console.log('responce code::' + response.statusCode);
        process.exit(0);
    });
}