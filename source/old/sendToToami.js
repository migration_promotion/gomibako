function sendToToami() {
	if (msg.type == "infraRed") {
		var voltage = msg.payload * 5.0 / 256;
    	var distance = 0;
    	if (voltage >= 0.4 && voltage < 2.0) {
        	distance = (voltage - 0.1) / 56.6;
	    } else if (voltage >= 2.0 && voltage < 2.55) {
	        distance = (voltage - 0.9) / 33;
	    }
	    msg.payload = 1 / distance;
	}
	var data = {sdata:{}};
// 	msg.payload = 90 - msg.payload;
    msg.payload = 50;
	node.warn(msg.payload);
}
sendToToami();
return msg;