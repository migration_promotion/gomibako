var noble = require('noble');
var DEVICE_NAME = "PERCUL01";
var SERVICE_UUID = '50455243554c20014001001ec067360b';
//超音波
var SERVICE_CHARACTERISTIC_UUID2 = "50455243554c20014002001ec067360b";
//赤外線
var SERVICE_CHARACTERISTIC_UUID = "82a4a4f66d0411e8adc0fa7ae01bbebc";

var ultrasonic = null;
var infraRed = null;
var requestCount = 0;
var dustboxHeight = 90;

// 時間のログを出力するためのもの
var d = new Date();
var year  = d.getFullYear();
var month = d.getMonth() + 1;
var day   = d.getDate();
var hour  = ( d.getHours()   < 10 ) ? '0' + d.getHours()   : d.getHours();
var min   = ( d.getMinutes() < 10 ) ? '0' + d.getMinutes() : d.getMinutes();
var sec   = ( d.getSeconds() < 10 ) ? '0' + d.getSeconds() : d.getSeconds();
var logTime = '[' + year + '-' + month + '-' + day + ' ' + hour + ':' + min + ':' + sec + ']';

// ログ文言
var logHeigherThanDustbox = 'ゴミ箱の高さ以上の値が入力されました';
var logError = 'Error';

var getSensorData = function(callback) {
    noble.on('stateChange', function(state) {
        if (state === 'poweredOn') {
            noble.startScanning();
        } else {
            noble.stopScanning();
        }
    });

    noble.on('scanStart', function() {
      console.log(logTime + 'on -> scanStart');
    });

    noble.on('scanStop', function() {
      console.log(logTime + 'on -> scanStop');
    });

    noble.on('discover', function(peripheral) {
//	console.log('UUID is ' + peripheral.uuid);
        if(peripheral.uuid == '001ec067360b'){
	    console.log('connect');
            peripheral.connect(function(error) {
	        noble.stopScanning();
                console.log(logTime + 'connected to peripheral: ' + peripheral.uuid);
                peripheral.discoverServices(SERVICE_UUID, function(error, services) {
		    console.log('services ' + services);
                    var deviceInformationService = services[0];
                    var result = "";
                    deviceInformationService.discoverCharacteristics(SERVICE_CHARACTERISTIC_UUID2, function(error, characteristics) {
                        for(j = 0; j < characteristics.length; j++) {
                            if(SERVICE_CHARACTERISTIC_UUID2  == characteristics[j].uuid ){
                                console.log(logTime + "SERVICE_CHARACTERISTIC_UUID2 exist!!");
                                characteristics[j].on('data', function(data, isNotification) {
                                    result += data[0];
                                    ultrasonic = result;
                                    sendUltrasonic(result);
                   	 	    peripheral.disconnect();
                                });
                                characteristics[j].subscribe(function(error) {
                                    console.log(logTime + 'notify');
                                });
                            }
                            if(SERVICE_CHARACTERISTIC_UUID  == characteristics[j].uuid ){
                                console.log(logTime + "SERVICE_CHARACTERISTIC_UUID exist!!");
                                characteristics[j].on('data', function(data, isNotification) {
                                    result = 0
                                    result += data[0];
                                    infraRed = result;
                    		    sendInfraRed(result);
                    		    peripheral.disconnect();
                                });
                                characteristics[j].subscribe(function(error) {
                                    console.log(logTime + 'notify');
                                });
				if (ultrasonic !== null) {
				    callback([ultrasonic, ultrasonic]);
				}
                                if (ultrasonic !== null && infraRed !== null) {
                                    callback([ultrasonic, infraRed]);
                                }
                            }
                        }
                        //return [ultrasonic, infraRed];
                    });
                });
            });
//	}
        peripheral.on('disconnect', function() {
            console.log(logTime + 'on -> disconnect');
            //if (ultrasonic === null && infraRed === null) {
            //    process.exit(0);
            //}
	    noble.startScanning();
        });
       }
    });
}
getSensorData(function(sensorData){
    console.log(logTime + 'getSensorData');
});

function sendUltrasonic (ultrasonic) {
    var data = ultrasonic.replace(",","");
    if (ultrasonic > dustboxHeight) {
    console.log(logTime + logError + '::超音波：' + logHeigherThanDustbox + '(' + ultrasonic  + ')');
        //process.exit(0);
    }
    sensorData = {};
    sensorData.timestamp = new Date().toISOString();
    console.log(logTime + 'データ:' + data);
          sensorData = {
                sdata:{
                        n01: dustboxHeight - ultrasonic
                }
          }
    console.log(logTime + '超音波:' + sensorData['sdata']['n01']);
    SendDataToToami(sensorData);
    return;
}

function sendInfraRed (infraRed) {
    var voltage = infraRed * 5.0 / 256;
    var distance = 0;
    if (voltage >= 0.4 && voltage < 2.0) {
        distance = (voltage - 0.1) / 56.6;
    } else if (voltage >= 2.0 && voltage < 2.55) {
        distance = (voltage - 0.9) / 33;
    }
    console.log(logTime + "入力値:" + infraRed)
    console.log(logTime + voltage);
    console.log(logTime + 'distance:' + distance);
    console.log(logTime + '距離(cm)' + 1/distance);

    var result = (infraRed / 16) * 10 + infraRed % 16;
    if (1/distance > dustboxHeight) {
        console.log(logTime + logError + "::赤外線：" + logHeigherThanDustbox);
 //       process.exit(0);
    }
    var sensorData = {};
    sensorData.timestamp = new Date().toISOString();
    sensorData = {
        sdata:{
            n02: dustboxHeight - 1/distance
        }
    }
    console.log(logTime + '赤外線:' + sensorData['sdata']['n02']);
    SendDataToToami(sensorData);
    return;
}

//  ------- Toamiに投げる部分 --------
// ↓の行を追加
var request = require("request");
// send to toami
function SendDataToToami(data){
    var headers = {
        "appkey":"acc38dde-fdfa-4ab6-a699-7b4e72166727",
        "Content-Type":"application/json;charset=UTF-8",
        "Accept":"application/json"
    };
    var options = {
        url: "https://demo01.to4do.com/Thingworx/Things/Thg_GW_kyxa_9xlt/Properties/sdata",
        method: "PUT",
        headers: headers,
        json: data
    };

    request(options, function(error,response){
        console.log(logTime + 'responce code::' + response.statusCode);
        requestCount += 1;
        if (requestCount > 1) {
            console.log(logTime + 'exit');
//            process.exit(0);
        } 
    });
}
