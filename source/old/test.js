'use strict';

const noble = require('noble');
const serviceuuid = `82a49d446d0411e8adc0fa7ae01bbebc`;
const charauuid = `82a4a0006d0411e8adc0fa7ae01bbebc`;



//discovered BLE device
const discovered = (peripheral) => {
    console.log(`BLE Device Found: ${peripheral.advertisement.localName}(${peripheral.uuid}) RSSI${peripheral.rssi}`);

    if(peripheral.advertisement.localName === 'PERCUL01'){
        noble.stopScanning();
        console.log('device found');
        console.log(`service discover...`);

        peripheral.connect(error => {
            if (error) {
                console.log("connection error:", error)
            } else {
                console.log("device connected");
            }
			console.log("connect");


            peripheral.discoverServices([],(err, services) => {
                if (error) {
                    console.log("discover service error", error)
                }
                console.log('discover service');
                services.forEach(service => {
                    if(service.uuid === serviceuuid){
                        service.discoverCharacteristics([], function(error, characteristics) {

	              		//get notify data
        	      		characteristics[0].on('data', function(data, isNotification) {
               				var result = "";
               				for(var i=0; i<data.length; i++){
                  				result += data[i] + ',';
                			}
                			testFunction(result);
              			});
              			characteristics[0].subscribe(function(error) {
                			console.log('notify');
              			});
                        	console.log('discover chara');
                        });
                    }
                });
            });
        });
    }
}

//BLE scan start
const scanStart = () => {
    noble.startScanning();
    noble.on('discover', discovered);
}

if(noble.state === 'poweredOn'){
    scanStart();
}else{
    noble.on('stateChange', scanStart);
}


// send Data
function testFunction (result) {
    var data = result.replace(",","");
    console.log(data);
    return;
}
