# install node-red-contrib-noble & noble
cd /var/webui/app/nodered/
npm install node-red-contrib-noble noble

# copy node-red.conf
cp ~/iothandson/gomi/source/node-red.conf /etc/supervisor/conf.d/node-red.conf
# copy settings.js
cp ~/iothandson/gomi/source/settings.js /var/webui/app/nodered/node_modules/node-red/settings.js

# copy advertise
mkdir /var/webui/app/nodered/node_modules/node-red/nodes/core/nak/
cp ~/iothandson/gomi/source/advertise.js /var/webui/app/nodered/node_modules/node-red/nodes/core/nak/advertise.js
cp ~/iothandson/gomi/source/advertise.html /var/webui/app/nodered/node_modules/node-red/nodes/core/nak/advertise.html

# copy flows
cp ~/iothandson/gomi/source/node.json /var/webui/app/nodered/workdir/flows_obsiot.example.org.json


