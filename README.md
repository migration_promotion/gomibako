# OpenBlocks設定
## 初期設定
OpenBlocksのSSIDとAPN設定、NODERED有効化、SSHの効化を設定する

1. 設定端末(MacBookなど)でOpneBlocksのWifiに接続
![](screenshot/selectSSID.png)

0. 設定端末のブラウザURLに192.168.254.254:880を入力し表示
![](screenshot/displayLogin.png)

0. OpenBlocks設定画面へのログイン
- ID/Passはroot/root
![](screenshot/displayTop.png)

4. SSIDの設定
- 画面上部タブ **ネットワーク>基本** からネットワーク設定画面を表示
- SSID:任意(本体裏のシリアル番号:G8E00130等にしておく)
- パスフレーズ:任意(自動にした場合は必ず控える、任意の場合は8文字以上のポリシー)
![](screenshot/settingSSID.png)

5. モバイル回線APNの設定
PoC案件は「mopera.net」なることを確認(契約するため)
![](screenshot/settingAPN.png)

6. OpenBlocks再起動
画面上部タブ **メンテナンス>停止・再起動** の再起動ボタンをクリック
![](screenshot/executeReboot.png)

7. SSHとNODEREDのポートフィルター解除
画面上部タブ **システム>フィルター** からフィルター解除
![](screenshot/settingFilter.png)

8. NODEREDの有効化
画面上部タブ **拡張>NODERED** からNODEREDを有効化
![](screenshot/activateNODERED.png)

9. OpenBlocks再起動
画面上部タブ **メンテナンス>停止・再起動** の再起動ボタンをクリック
![](screenshot/executeReboot.png)

## node.js設定およびプログラムの配置
bitbucket(git)からセットアップ用のスクリプトをcloneする

1. OpenBlockにsshログイン
設定端末(MacBookなど)で以下コマンド実行
```
# ssh root@192.168.254.254
パスワード:0BSI0T
 ```

2. bitbucketからセットアップスクリプトおよびソースダウンロード
設定端末(MacBookなど)で以下コマンド実行
```
# git clone https://iothandson2@bitbucket.org/iothandson2/iothandson.git
パスワード:1qaz0plm
```
3. セットアップスクリプトの実行
設定端末(MacBookなど)で以下コマンド実行
```
# sh iothandson/gomi/setup.sh
```
    - 実行内容は以下
        - npmでnode-red-contrib-noble & nobleのインストール 
        - source/node-red.conf & setting.jsのコピー
        - source/advertise.*をコピー
        - source/node.js(フローファイル)のコピー

4. NODERED機能の再起動
-  http://192.168.254.254:880/extension/nodered.php アクセスしてNode-REDを再起動
- 無効化→保存→無効化→保存で機能の再起動
![](screenshot/rebootNODERED.png)

## advertise設定
node.jsでadvertiseを使用できるよう設定
1. URL http://192.168.254.254:1880/# にアクセスしてadvertise内の設定情報
    - 詳細はIoTマイグレ推進 渋谷さんに

# Toami(WEB) - GW設定等
1. 機器管理タブ「GW設定」にてGWを作成する
2. 認証されるときお作法
	- GW名称:任意10文字以下
	- GWグループ:設定無し(今の所)
	- 設置場所:任意の文字列
	- URL,ゲートウェイネーム,appkeyが生成されるので(画面左の作成したGWNo.をクリックすると生成情報が表示される
	- getData ノードのところに記述があるのでToamiの接続先(IDとパスワード)が決まったら変更する

# Toami(WEB) - 機器管理タブ、機器センサー設定
- 画面で設定可能
- REST番号がデータの入る記述
	- エクスポートは設定内容のcsvが出力できるだけ
- モニタリングタブで画面をつくる(ダッシュボードグループを追加、ガジェットを追加)

## TODO
- UUID(MAC ADDR)をSearchUUIDノードにOpenBlocksごとに設定が必要
- REST番号がデータの入る記述
	- エクスポートは設定内容のcsvが出力できるだけ
- モニタリングタブで画面をつくる(ダッシュボードグループを追加、ガジェットを追加)

